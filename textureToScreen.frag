#version 330

in vec2 colorCoord;
out vec4 outputColor;

uniform sampler2D colorTexture;

void main()
{
	outputColor = texture2D(colorTexture, colorCoord);
}